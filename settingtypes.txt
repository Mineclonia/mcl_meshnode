meshnode_max_speed (Meshnode entity maximum speed) float 8.0
meshnode_max_lift (Meshnode entity maximum lift) float 4.0
meshnode_yaw_amount (Meshnode entity turning step size) float 0.017
meshnode_max_radius (Meshnode entity maximum radius) int 8
meshnode_show_in_creative (Meshnode entity in creative inventory) bool false
meshnode_enable_crafting (Meshnode controller craftable) bool false
meshnode_disable_privilege (Meshnode privilege not needed for interaction) bool false
meshnode_fake_shading (Meshnode fake shading) bool false
meshnode_autoconf (Allow 4096 statically stored objects in a mapblock) bool false
meshnode_use_archimedes_ships_crafting_recipe (Use Archimedes' Ships crafting recipe) bool false
